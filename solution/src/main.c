
#include "../include/file.h"
#include "../include/bmp.h"

int main(int argc, char** argv) {
    (void) argc; (void) argv;
    if (argc != 3) {
        fprintf(stderr, "Not enough command line arguments. "
                        "The following arguments are required for the program to work correctly:"
                        "./image-transformer <source-image> <transformed-image>");
        return 1;
    }

    FILE* file;
    enum file_status file_status = file_open(&file, argv[1], "r");

    if (file_status != FILE_SUCCESS) {
        file_print_status(file_status, argv[1]);
        return 1;
    }

    struct image source_image;
    enum bmp_status bmp_status = from_bmp(file, &source_image);

    file_status = file_close(&file);

    if (file_status != FILE_SUCCESS) {
        image_destroy(&source_image);
        file_print_status(file_status, argv[1]);
        return 1;
    }

    if (bmp_status != BMP_SUCCESS) {
        image_destroy(&source_image);
        bmp_print_status(bmp_status, argv[1]);
        return 1;
    }

    struct image rotated_image;
    image_rotate(&source_image, &rotated_image);

    file_status = file_open(&file, argv[2], "w");

    if (file_status != FILE_SUCCESS) {
        image_destroy(&source_image);
        image_destroy(&rotated_image);
        file_print_status(file_status, argv[1]);
        return 1;
    }

    bmp_status = to_bmp(file, &rotated_image);

    file_status = file_close(&file);

    if (file_status != FILE_SUCCESS) {
        image_destroy(&source_image);
        image_destroy(&rotated_image);
        file_print_status(file_status, argv[1]);
        return 1;
    }

    if (bmp_status != BMP_SUCCESS) {
        image_destroy(&source_image);
        image_destroy(&rotated_image);
        bmp_print_status(bmp_status, argv[1]);
        return 1;
    }

    image_destroy(&source_image);
    image_destroy(&rotated_image);

    return 0;
}
