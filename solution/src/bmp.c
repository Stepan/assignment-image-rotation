#include "../include/bmp.h"
#include <stdlib.h>
void bmp_print_status(enum bmp_status status, char* filename){ //вывод ошибок для статусов
    switch (status) {
        case BMP_INVALID_HEADER:
            fprintf(stderr, "The file <%s> does not match the BMP-format (invalid header)", filename);
            break;
        case BMP_ERROR:
            fprintf(stderr, "The file <%s> does not match the BMP-format", filename);
            break;
        case BMP_SUCCESS:
            break;
    }
}

uint8_t padding(uint64_t width){ //расчет количества нулевых битов, которые нужно добавить в каждую строку пикселей
    if (width % 4 == 0) return 0;
    return 4 - (width * sizeof(struct pixel)) % 4;
}

enum bmp_status from_bmp(FILE* file, struct image* image) {
    struct bmp_header header;
    rewind(file); // устанавливает указатель в начало файла
    size_t read_elem_num = fread(&header, sizeof(struct bmp_header), 1, file); //чтение bmp заголовка

    if (read_elem_num != 1) return BMP_ERROR; //проверка bmp заголовка

    unsigned char b_byte = header.bfType & 0xFF;
    unsigned char m_byte = (header.bfType >> 8) & 0xFF;
    if (b_byte != 'B' || m_byte != 'M') return BMP_INVALID_HEADER;

    uint64_t width = header.biWidth; //установка значений из созданной структуры
    uint64_t height = header.biHeight;

    struct pixel* pixel_array = pixel_alloc_array(height * width); //создание массива пикселей
    if (pixel_array == NULL) return BMP_ERROR;

    uint8_t pad = padding(width); //получение отступа

    for (uint64_t i = 0; i < height; i++) { //построчное чтение массива пикселей из bmp
        fread(pixel_array + i * width, sizeof(struct pixel) * width, 1 , file);
        fseek(file, pad, SEEK_CUR); //перемещение указателя потока через отступ
    }
    image->height=height;
    image->width=width;
    image->data=pixel_array;

    return BMP_SUCCESS;
}

enum bmp_status to_bmp(FILE* file, struct image* image) {

    struct bmp_header header = bmp_create_header(image->width, image->height); //создание заголовка bmp

    fwrite(&header, sizeof(struct bmp_header), 1, file); //запись заголовка в файл
    fseek(file, header.bOffBits, SEEK_SET);

    uint8_t pad = padding(image->width); //получение отступа
    uint8_t* padding_to_bmp;
    if (pad != 0) padding_to_bmp = calloc(1, pad); //инициализация отступа через создание блока памяти заполненного нулями

    for (uint64_t i = 0; i < image->height; i++){ //запись хэдэра и массива пикселей в файл
        fwrite(image->data + image->width * i, image->width * sizeof(struct pixel), 1, file);
        if (pad != 0) fwrite(padding_to_bmp, pad, 1, file);
    }

    if (pad != 0) free(padding_to_bmp); //очистка памяти выделенной под отступ

    return BMP_SUCCESS;
}

struct bmp_header bmp_create_header(uint64_t width, uint64_t height) {
    const uint32_t head_size = sizeof(struct bmp_header);
    const uint32_t img_size = sizeof(struct pixel) * height * (width + padding(width));
    const uint32_t file_size = head_size + img_size;

    return (struct bmp_header) {
            .bfType = 0x4D42,
            .bfileSize = file_size,
            .bfReserved = 0,
            .bOffBits = head_size,
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = img_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}
