#include "../include/image.h"
#include <stdlib.h>

struct pixel* pixel_alloc_array(size_t size) {
    return malloc(size * sizeof(struct pixel));
}
struct image image_create(uint64_t width, uint64_t height) {
    return (struct image) {width, height, malloc(sizeof(struct pixel) * width * height)};
}

void image_destroy(struct image* const image) {
    image->width = 0;
    image->height = 0;
    if (image->data != NULL) free(image->data);
}

struct pixel image_get_pixel(struct image const* const image, uint64_t width, uint64_t height) {
    size_t pixel_index = width + height * image->width;
    return image->data[pixel_index];
}

void image_set_pixel(struct image* const image, struct pixel pixel, uint64_t width, uint64_t height) {
    image->data[height * image->width + width] = pixel;
}

void image_rotate(struct image* const source_image, struct image* const rotated_image) {
    uint64_t rotated_width = source_image->height;
    uint64_t rotated_height = source_image->width;

    rotated_image->width=rotated_width;
    rotated_image->height=rotated_height;
    struct pixel* pixel_array = pixel_alloc_array(rotated_width * rotated_height);
    rotated_image->data=pixel_array;

    for (uint64_t i = 0; i < rotated_height; i++) {
        for (uint64_t j = 0; j < rotated_width; j++) {
            struct pixel pixel = image_get_pixel(source_image, i, rotated_width - j - 1);
            image_set_pixel(rotated_image, pixel, j, i);
        }
    }
}


