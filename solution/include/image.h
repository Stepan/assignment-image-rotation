//
// Created by Stepan on 30.10.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#include <stddef.h>
#include <stdint.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel {uint8_t b, g, r;};

struct pixel* pixel_alloc_array(size_t size);

struct image image_create(uint64_t width, uint64_t height);

struct pixel image_get_pixel(struct image const* const image, uint64_t width, uint64_t height);

void image_set_pixel(struct image* const image, struct pixel pixel, uint64_t width, uint64_t height);

void image_rotate(struct image* const source_image, struct image* const rotated_image);

void image_destroy(struct image* const image);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
